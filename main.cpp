#include <iostream>
using namespace std;

int main()
{   
    const int N = 10;
    int mas[N][N];
    int sum = 0;
    for (int i = 0; i < N; i++) // Заполнение массива
        for (int j = 0; j < N; j++)
            mas[i][j] = i + j;
    for (int i = 0; i < N; i++) // Вывод массива
        for (int j = 0; j < N; j++)
            cout << mas[i][j] << " ";
    cout << endl;
    for (int i = 0; i < N; i++) // Сумма элементов из каждой строки с индексом 2 % N
        sum += mas[i][2 % N];
    cout << sum << endl;
    return 0;
}